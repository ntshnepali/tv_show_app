import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import Header from "./components/Header";
import SimpleBottomNavigation from "./components/MainNav";
import Movies from "./Pages/Movie/Movie";
import Home from "./Pages/Home/Home";
import TvShows from "./Pages/TvShows/TvShows";
import Search from "./Pages/Search/Search";
import { Container } from "@material-ui/core";
function App() {
  return (
    <BrowserRouter>
      <Header />
      <div className="app">
        <Container>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/movies" component={Movies} />
            <Route path="/TvShows" component={TvShows} />
            <Route path="/Search" component={Search} />
          </Switch>
        </Container>
      </div>
      <SimpleBottomNavigation />
    </BrowserRouter>
  );
}

export default App;
